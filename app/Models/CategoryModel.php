<?php
namespace App\Models;
use CodeIgniter\Model;
class CategoryModel extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'parent_id'];
    public function categories()
    {
        return $this->belongsTo('categories', 'App\Models\CategoryModel','parent_id','id');
        // $this->belongsTo('propertyName', 'model', 'foreign_key', 'owner_key');
    }


}