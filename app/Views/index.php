<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Codeigniter 4 </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<div class="container mt-4">
    <div class="d-flex justify-content-end">
        <a href="<?php echo site_url('/category-form') ?>" class="btn btn-success mb-2">Add Category</a>
    </div>
    <?php

        $session = \Config\Services::session();

        if($session->getFlashdata('msg'))
        {
            echo '
            <div class="alert alert-success">'.$session->getFlashdata("msg").'</div>
            ';
        }
        ?>
    <div class="mt-3">
        <table class="table table-bordered" id="users-list">
            <thead>
            <tr>
                <th> Id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <?php if($categories): ?>
                <?php foreach($categories as $category): ?>
                    <tr>
                        <td><?php echo $category['id']; ?></td>
                        <td><?php echo $category['name']; ?></td>
                        <td>
                            <a href="<?php echo base_url('delete/'.$category['id']);?>" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#users-list').DataTable();
    } );
</script>
</body>
</html>