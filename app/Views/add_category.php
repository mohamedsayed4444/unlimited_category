<!DOCTYPE html>
<html>
<head>
    <title>Codeigniter 4 Add User With Validation Demo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        .container {
            max-width: 500px;
        }
        .error {
            display: block;
            padding-top: 5px;
            font-size: 14px;
            color: red;
        }
    </style>
</head>
<body>
<div class="container mt-5">
    <form method="post" id="add_create" name="add_create"
          action="<?= site_url('/submit-form') ?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control">
        </div>

        <div class="form-group">
            <label>Category</label>
            <select class="form-control" name="category" id="category" required>
                <option value="">No Selected</option>
                <?php foreach($categories as $row):?>
                    <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group">
            <label>Sub Category</label>
            <select class="form-control" id="sub_category" name="sub_category" >
                <option>No Selected</option>

            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Update Data</button>
        </div>
    </form>
</div>
<!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>-->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#category').change(function(){
            console.log('ffff');
            var id=$(this).val();
            $.ajax({
                url : "<?php echo site_url('get_sub_category');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){

                    var html = '<option ></option>';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                    }
                    $('#sub_category').html(html);

                }
            });
            return false;
        });

    });
</script>
</body>
</html>