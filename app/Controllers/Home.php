<?php

namespace App\Controllers;

use App\Models\CategoryModel;

class Home extends BaseController
{
//    function __construct(){
//        parent::__construct();
//        $this->load->model('Product_model','product_model');
//    }

    function index(){
        $category = new CategoryModel();
        $data['categories'] = $category->orderBy('id', 'DESC')->findAll();
//        $this->load->view('product_view', $data);
        return view('index',$data);
    }
    public function create(){
        $category = new CategoryModel();
        $data['categories'] = $category->orderBy('id', 'DESC')->findAll();
        return view('add_category',$data);
    }
    // get sub category by category_id
    function get_sub_category(){

        $category_id = $this->request->getVar('id');
//        $category_id = $this->input->post('id',TRUE);
        $category = new CategoryModel();
        $data = $category->where('parent_id',$category_id)->orderBy('id', 'DESC')->findAll();
        echo json_encode($data);
    }

    // insert data
    public function store() {
        $categoryModel = new CategoryModel();
        $name= $this->request->getVar('name');
        if ($this->request->getVar('sub_category')!=''){
            $parent_id=$this->request->getVar('sub_category');
        }
        $data = [
            'name' => $name,
            'parent_id'  => $parent_id,
        ];
        $categoryModel->insert($data);
        $session = \Config\Services::session();

        $session->setFlashdata('msg', 'category added successfully');
        return $this->response->redirect(site_url('/'));
    }


    // delete category
    public function delete($id = null)
    {
        $categoryModel = new CategoryModel();
        $data['category'] = $categoryModel->where('id', $id)->delete($id);

        $session = \Config\Services::session();

        $session->setFlashdata('msg', 'category deleted successfully');
        return $this->response->redirect(site_url('/'));

    }
//    function get_sub_category(){
//        $category_id = $this->input->post('id',TRUE);
//        $data = $this->product_model->get_sub_category($category_id)->result();
//        echo json_encode($data);
//    }

}
